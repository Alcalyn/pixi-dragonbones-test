class Coder extends BaseDemo {
    public constructor() {
        super();

        this._resources.push(
            "dragonbones-export/Coder_ske.json",
            "dragonbones-export/Coder_tex.json",
            "dragonbones-export/Coder_tex.png",
        );
    }

    protected _onStart(): void {
        const factory = dragonBones.PixiFactory.factory;

        factory.parseDragonBonesData(this._pixiResources["dragonbones-export/Coder_ske.json"].data);
        factory.parseTextureAtlasData(
            this._pixiResources["dragonbones-export/Coder_tex.json"].data,
            this._pixiResources["dragonbones-export/Coder_tex.png"].texture
        );

        const armatureDisplay = factory.buildArmatureDisplay("Armature");
        armatureDisplay.animation.play("Wriggle");

        armatureDisplay.x = 0.0;
        armatureDisplay.y = 50.0;
        armatureDisplay.scale.x = 0.4;
        armatureDisplay.scale.y = 0.4;

        this.addChild(armatureDisplay);
    }
}
